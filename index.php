<?php
/**
 * Created by PhpStorm.
 * User: Eduardo
 * Date: 25/11/2016
 * Time: 20:04
 */

// Composer's auto-loading functionality
require "vendor/autoload.php";

use Dompdf\Dompdf;

/**
 * Directorio donde se encuentran los html de origen
 */
$PATH_SOURCE = ".\\source\\";
/**
 * Directorio donde se guardaran los PDFs generados
 */
$PATH_OUTPUT = $_SERVER['DOCUMENT_ROOT']."renderpdf/output/";


//Leer la carpeta y obtener el numero de archivos.
if ($handle = opendir($PATH_SOURCE)) {
    echo 'Archivos: ' . count($handle) . '<br/>';

    $procesados = 0;

    while (false !== ($entry = readdir($handle))) {

        if ($entry != "." && $entry != "..") {
            if ($entry == "index.php") continue;

          //leer el contenido de cada archivo.
            $html =  file_get_contents ( $PATH_SOURCE.$entry);
            $dompdf = new DOMPDF();  //if you use namespaces you may use new \DOMPDF()
            //carga el contenido en el objeto DOMPDF
            $dompdf->loadHtml($html);
            //renderiza el objeto.
            $dompdf->render();
            //Genera el archivo
            $pdf = $dompdf->output();
            $file_location = $PATH_OUTPUT.$entry;
            //se cambia crea el archivo con el mismo nombre pero con extension .PDF
            $file_location = $PATH_OUTPUT.pathinfo($file_location, PATHINFO_FILENAME) . ".pdf";
            //echo $file_location;
            //guarda el archivo en el directorio
            $result = file_put_contents($file_location,$pdf);
            if ($result != false) {
                //si se guarda correctamente se incrementa el contador de los archivos procesados.
                $procesados++;
            }

        }
    }
    echo 'Se han procesado todos los archivos...';
    //cierra el directorio.
    closedir($handle);
}
