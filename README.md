<h1>RederPdfMass</h1>

<p>Es una aplicacion sencilla en php que se encarga de generar pdf de forma masiva, 
se leen todos los archivos que estan contenidos dentro de la carpeta de 
origen "source", se renderiza a PDF y el resultado es guardado con el mismo nombre
pero transformado en pdf en el directorio de Salida <b>"output"</b><p>

<ul>
<li>/source: En esta carpeta se deben colocar todos los archivos html </li>
<li>/output: El resultado de los archivos PDFs generados se guardan en este directorio. </li>
<li>/index.php </li>
</ul>
